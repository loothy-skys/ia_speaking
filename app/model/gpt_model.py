from dotenv import load_dotenv
import openai
import os
from typing import Optional
# Chargez les variables d'environnement une seule fois au début du script.


class GptModel:
    
    def __init__(self, text : str) -> None:
        load_dotenv()
        openai.api_key = os.getenv('OPENAI_API_KEY')
        self.text = text
        
    def get_gpt_response(self) -> Optional[str]:
        """Génère une réponse en utilisant le modèle GPT."""
        try:
            response = openai.ChatCompletion.create(
                model="gpt-3.5-turbo",  # Assurez-vous que 'gpt-3.5-turbo' est valide et disponible
                messages=[
                    {"role": "system", "content": "loading message .."},
                    {"role": "user", "content": self.text}
                ]
            )
            
            # Récupérer le contenu du message de la réponses
            response_content = response.choices[0].message['content'] if response.choices else None
            
            # Affiche la réponse à la console pour le debug
            print('Réponse obtenue:', response_content)
            
            return response_content
        
        except Exception as e:
            # Gérer l'exception si quelque chose tourne mal lors de l'appel à l'API
            print(f"Une erreur s'est produite: {e}")
            return None
