
from gtts import gTTS
from pydub import AudioSegment
from pydub.playback import play
from pydub.effects import speedup



class TextToSpeechModel:
    
    
    def __init__(self, text_to_speech: str)-> None:
        
        self.text_to_speech = text_to_speech
        
    def get_text_to_speech(self)-> None:
        
        language = "fr"
        textSpeech = gTTS(text=self.text_to_speech, lang=language, slow=False)
        textSpeech.save("C:/JuLiA/ialecture.mp3")
        audio = AudioSegment.from_file("C:/JuLiA/ialecture.mp3", format="mp3")
        speed = speedup(audio, 1.25)
        # play the audio
        play(speed)