import warnings
import speech_recognition as sr

class SpeechToTextModel:
    def __init__(self)-> None:
        warnings.simplefilter("ignore")
        self.r = sr.Recognizer()
        warnings.filterwarnings('module', 'always', )
  
        
    def get_speech_to_text(self)-> str:
        
        
        with sr.Microphone() as source:
            
            audio = self.r.listen(source)
        text = self.r.recognize_google(audio, None, "fr-FR")
  
        return text