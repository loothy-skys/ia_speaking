
from app.model.speech_to_text_model import SpeechToTextModel
from app.model.text_to_speech_model import TextToSpeechModel
from app.services.whatsapp_service import WhatsAppService
from app.model.gpt_model import GptModel

class SpeechControler:
    
    def __init__(self)-> None:
        self.speech_to_text_model = None
        self.gpt_model = None
        self.text_to_speech_model = None
     
        
    def run(self)-> None:
        # Créer l'instance de SpeechToTextModel et obtenir la sortie nécessaire.
        self.speech_to_text_model = SpeechToTextModel()
        speech_to_text_output = self.speech_to_text_model.get_speech_to_text()
    
            
        # Créer l'instance de GptModel avec la sortie de l'étape précédente.
        self.gpt_model = GptModel(speech_to_text_output)
        gpt_response = self.gpt_model.get_gpt_response()

        # Créer l'instance de TextToSpeechModel avec la sortie de l'étape précédente.
        self.text_to_speech_model = TextToSpeechModel(gpt_response)
        self.text_to_speech_model.get_text_to_speech()