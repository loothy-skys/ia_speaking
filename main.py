

from app.controleur.speech_controller import SpeechControler


def main()-> None:
    while True:
        # Création d'une instance du contrôleur principal et démarrage de l'application.
        controller = SpeechControler()
    
        controller.run()
        
if __name__ == "__main__":
    main()