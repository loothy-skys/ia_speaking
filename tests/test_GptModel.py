import unittest
from unittest.mock import patch
from iaservant.jarvis.AI_speak.app.model.gpt_model import GptModel



class TestGptModel(unittest.TestCase):

    @patch('app.model.gpt_model')
    def test_get_gpt_response_success(self, mock_chat_completion):
        # Configurer le mock pour retourner une réponse simulée d'OpenAI
        mock_chat_completion.return_value = {
            'choices': [
                {'message': {'content': 'tu dois dire une phrase avec les mots : Salut à toi, jeune entrepreneur'}}
            ]
        }
        
        gpt_model = GptModel()
        response = gpt_model.get_gpt_response("tu dois dire une phrase avec les mots : Salut à toi, jeune entrepreneur")
        
        # Vérifier que la réponse est correcte
        self.assertIn("Salut à toi, jeune entrepreneur", response)
  

if __name__ == '__main__':
    unittest.main()