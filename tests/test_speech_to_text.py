import pytest
from unittest.mock import patch, MagicMock
from app.model.speech_to_text_model import SpeechToText

@pytest.fixture()
def audio_fixture():
    # Cette fixture simule la présence d'un objet 'AudioData'
    class AudioDataMock(MagicMock):
        pass
    return AudioDataMock()

# Vous devez spécifier les chemins corrects pour mocker 'Recognizer' et 'Microphone'
@patch('speech_recognition.Recognizer')
@patch('speech_recognition.Microphone')
def test_get_speech_to_text(mic_mock, recognizer_mock, audio_fixture):
    # Configurez le mock recognizer pour utiliser notre fixture audio
    recognizer_instance = recognizer_mock.return_value
    recognizer_instance.listen.return_value = audio_fixture
    
    # Configurez le mock recognizer pour renvoyer un texte simulé lorsqu'il reconnaît l'audio
    expected_text = "ceci est un test"
    recognizer_instance.recognize_google.return_value = expected_text
    
    # Testez la fonctionnalité
    stt = SpeechToText()
    result = stt.get_speech_to_text()
    print(result)
    # Vérifiez que le texte attendu est retourné
    assert result == expected_text

    # Assurez-vous que les appels ont été faits
    recognizer_instance.listen.assert_called_once()
    recognizer_instance.recognize_google.assert_called_once_with(audio_fixture, None, "fr-FR")